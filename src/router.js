import React from 'react';
import { Router, Scene, Stack, Actions } from 'react-native-router-flux';
import Login from './login/login.js';
import Home from './home/home.js';
import ListView from './listView/listView.js'
import {
    Button,
    Text,
    View
} from 'react-native';
const Routers = () => {
    return (
        <Router>
            <Stack key="root">
                <Scene
                    hideNavBar={true}
                    key="login"
                    component={Login}
                    title="login"
                    initial={true}
                />
                <Scene
                    hideNavBar={true}
                    key="home"
                    component={Home}
                    title="home"
                />
                <Scene
                    hideNavBar={true}
                    key="listview"
                    component={ListView}
                    title="listview"
                />
            </Stack>
        </Router>
    );
}

export default Routers;