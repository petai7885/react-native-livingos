import React, { Component, useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    Image,
    Linking,
    TouchableHighlight,
    Platform,
    ScrollView
} from 'react-native';
import { Actions } from 'react-native-router-flux'; // New code
import { Header } from 'react-native-elements';
import react from '../../assets/reacr.png'
import LivingOs from '../../assets/livingOs.png'
import GoogleMap from '../../assets/map.png'
import Images from '../../assets/photo.png'
import ListView from '../../assets/listview.png'
import ountain from '../../assets/mountain.jpg'
import ImageView from 'react-native-image-view';


const handlePress = () => {
    let url = 'https://www.thelivingos.com/'
    Linking.openURL(url);
}
const openGooglemap = () => {
    if (Platform.OS === 'ios') return Linking.openURL('maps://app')
    return Linking.openURL('google.navigation:')
}

const images = [
    {
        source: {
            uri: 'https://images.unsplash.com/photo-1573273787173-0eb81a833b34',
        },
        title: 'Paris',
        width: 806,
        height: 720,
    },
];



const ScarletScreen = () => {
    const [onImage, setOnImage] = useState(false);
    return (
        <View>
            <ImageView
                images={images}
                imageIndex={0}
                isVisible={onImage}
                isSwipeCloseEnabled={false}
                onClose={() => setOnImage(false)}
            />
            <Header
                leftComponent={<></>}
                centerComponent={{ text: 'HOME', style: { color: '#fff' } }}
                rightComponent={<Button
                    title="Right button"
                    onPress={() => Actions.reset('login')}
                    title="logout"
                />}
            />
            <ScrollView>
                <View style={styles.flexBody1}>
                    <TouchableHighlight onPress={() => handlePress()}>
                        <Image style={styles.imageLo} source={LivingOs} />
                    </TouchableHighlight>

                    <TouchableHighlight onPress={() => openGooglemap()}>
                        <Image style={styles.imageM} source={GoogleMap} />
                    </TouchableHighlight>
                </View>
                <View style={styles.flexBody2}>
                    <TouchableHighlight onPress={() => setOnImage(true)}>
                        <Image style={styles.imageI} source={Images} />
                    </TouchableHighlight>
                    <TouchableHighlight onPress={() => Actions.push('listview')}>
                        <Image style={styles.imageLS} source={ListView} />
                    </TouchableHighlight>
                </View>


            </ScrollView>

            {/* <Button
                    title="Right button"
                    onPress={() => Actions.push('listview')}
                    title="listview"
                /> */}
        </View >
    );
}

const styles = StyleSheet.create({
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
    },
    logout: {
        flex: 1,
        paddingTop: 15
    },
    page: {
        paddingLeft: 285
    },
    button: {
        width: 50,
        height: 50
    },
    flexBody1: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 200,
    },
    flexBody2: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 50,
    },
    imageLo: {
        flex: 1,
        width: 140,
        height: 140,
    },
    imageM: {
        flex: 1,
        width: 140,
        height: 140,
        paddingTop: 100,
        marginLeft: 50
    },
    imageI: {
        width: 140,
        height: 140,
        paddingTop: 100,
        paddingLeft: 50,
    },
    imageLS: {
        width: 140,
        height: 140,
        paddingTop: 100,
        marginLeft: 50
    },
    body: {
        justifyContent: 'center',
    },
    imageBackground: {
        flex: 2
    },
});

export default ScarletScreen;