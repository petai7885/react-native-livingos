
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
const Header = () => {
    return (
        <View style={styles.container}>
            <Text>
                Header
            </Text>
            <Text>
                My supercool header
            </Text>
        </View>
    );
}




const styles = StyleSheet.create({
    container: {
        padding: 60,
        textAlign: 'center',
        // background: '#1abc9c',
        // color: 'white',
        // fontSize: 30,
    }
});
export default Header;