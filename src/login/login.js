import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import uni from '../../assets/ดาวน์โหลด.png'
import { Header } from 'react-native-elements';






const pagrHome = (username, passwords) => {
    if (username && passwords) {
        Actions.reset('home')
    } else {
        alert('กรุณากรอกข้อมูลให้ครบ')
    }
}
const ScarletScreen = () => {

    const [username, onChangeUsername] = React.useState(null);
    const [password, onChangePassword] = React.useState(null);
    return (

        <View>
            <Header
                leftComponent={<></>}
                centerComponent={{ text: 'LOGIN', style: { color: '#fff', paddingTop: 8 } }}
                rightComponent={<></>}
            />
            <View style={styles.container}>
                <View style={styles.viewImage}>
                    <Image style={styles.image} source={uni} />
                </View>
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeUsername}
                    value={username}
                    placeholder="username"
                />
                <TextInput
                    style={styles.input}
                    onChangeText={onChangePassword}
                    value={password}
                    placeholder="password"
                />
                <View >
                    <Button
                        title="login"
                        onPress={() => pagrHome(username, password)}
                    />
                </View>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        paddingTop: 50,
        paddingLeft: 50,
        paddingRight: 50
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
    },
    image: {
        width: 200,
        height: 200,
        resizeMode: 'stretch',
        borderRadius: 100 / 2,
        paddingLeft: 50
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
    viewImage: {
        paddingLeft: 40
    }
});

export default ScarletScreen;