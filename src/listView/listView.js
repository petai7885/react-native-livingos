import React, { Component, useState, useEffect } from 'react';
import {
    Text,
    View,
    Button,
    ScrollView
} from 'react-native';
import { Actions } from 'react-native-router-flux'; // New code
import { Header, ListItem, Avatar } from 'react-native-elements';
const axios = require('axios').default;

const ListViews = () => {
    const [data, dataSet] = useState(null)
    useEffect(() => {
        async function fetchMyAPI() {
            let response = await fetch('https://jsonplaceholder.typicode.com/photos')
            response = await response.json()
            dataSet(response)
        }
        fetchMyAPI()
    }, [])
    return (
        <View>
            <Header
                leftComponent={<Button
                    title="Right button"
                    onPress={() => Actions.reset('home')}
                    title="back"
                />}
                centerComponent={{ text: 'LISTVIEW', style: { color: '#fff' } }}
                rightComponent={<Button
                    title="Right button"
                    onPress={() => Actions.reset('login')}
                    title="LOGOUT"
                />}
            />
            <ScrollView>
                {
                    data !== null ? data.map((el, i) => (
                        <ListItem key={i} bottomDivider>
                            <Avatar source={{ uri: el.thumbnailUrl }} />
                            <ListItem.Content>
                                <ListItem.Title>{el.title}</ListItem.Title>
                            </ListItem.Content>
                        </ListItem>
                    )) : <Text>

                    </Text>

                }
            </ScrollView>
        </View >
    );
}

export default ListViews;